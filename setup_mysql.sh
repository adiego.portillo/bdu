#!/usr/bin/env bash

ENV_FILE=".env"
BDU_DUMP_FILE="./mysql/initdb.d/bdu-dump.sql"
DEFAULT_MYSQL_HOST="localhost"
DEFAULT_MYSQL_ADMIN_USERNAME="root"
DEFAULT_MYSQL_ROOT_PASSWORD="secret"


MAX_MYSQL_DATABASE_NAME_LENGTH=64
MAX_MYSQL_USERNAME_LENGTH=16
declare -A REQUIRED_ENV_VARS=( [MYSQL_HOST]="" [MYSQL_DATABASE]="" [MYSQL_USER]="" [MYSQL_PASSWORD]="" [MYSQL_ADMIN_USERNAME]="" [MYSQL_ROOT_PASSWORD]="" )
# Exit script on first error
set -e
# Pretty colors!
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo "Prepeating database..."

# Verify .env file exists
confirm_env_file_exists() {
    while [ ! -f "${ENV_FILE}" ]; do
        if [ -z "${ENV_FILE}" ]; then
            printf "${RED}Error:${NC} Environment file not specified.\n"
        else
            printf "${RED}Error:${NC} Environment file \"$ENV_FILE\" does not exist.\n"
        fi
        echo -n "Enter the environment file name (e.g. \".env\"): "
        read ENV_FILE
        echo ""
    done
}

get_mysql_credentials() {
    MYSQL_HOST="${DEFAULT_MYSQL_HOST}"
    MYSQL_ADMIN_USERNAME="${DEFAULT_MYSQL_ADMIN_USERNAME}"
    MYSQL_ROOT_PASSWORD="${DEFAULT_MYSQL_ROOT_PASSWORD}"

    # Host name
    if [ -z "${DEFAULT_MYSQL_HOST}" ]; then
        while [ -z "${MYSQL_HOST}" ]; do
            echo -n "Enter the MySQL host name (e.g. localhost): "
            read MYSQL_HOST
        done
    else
        echo -n "Enter the MySQL host name (press [ENTER] for \"${DEFAULT_MYSQL_HOST}\"): "
        read MYSQL_HOST
        if [ -z "${MYSQL_HOST}" ]; then
            MYSQL_HOST="$DEFAULT_MYSQL_HOST"
        fi
    fi

    # Admin username
    if [ -z "${DEFAULT_MYSQL_ADMIN_USERNAME}" ]; then
        while [ -z "${MYSQL_ADMIN_USERNAME}" ]; do
            echo -n "Enter the MySQL admin username (e.g. root): "
            read MYSQL_ADMIN_USERNAME
        done
    else
        echo -n "Enter the MySQL admin username (press [ENTER] for \"${DEFAULT_MYSQL_ADMIN_USERNAME}\"): "
        read MYSQL_ADMIN_USERNAME
        if [ -z "${MYSQL_ADMIN_USERNAME}" ]; then
            MYSQL_ADMIN_USERNAME="$DEFAULT_MYSQL_ADMIN_USERNAME"
        fi
    fi

    # Admin password
    if [ -z "${DEFAULT_MYSQL_ROOT_PASSWORD}" ]; then
        while [ -z "${MYSQL_ROOT_PASSWORD}" ]; do
            echo -n "Enter the MySQL admin password (e.g. secret): "
            read MYSQL_ROOT_PASSWORD
        done
    else
        echo -n "Enter the MySQL admin password (press [ENTER] for \"${DEFAULT_MYSQL_ROOT_PASSWORD}\"): "
        read MYSQL_ROOT_PASSWORD
        if [ -z "${MYSQL_ROOT_PASSWORD}" ]; then
            MYSQL_ROOT_PASSWORD="$DEFAULT_MYSQL_ROOT_PASSWORD"
        fi
    fi
}

ERRORS_EXIST=false

if [[ -z "$ENV_FILE" || ! -f "${ENV_FILE}" ]]; then
    confirm_env_file_exists
fi

# Verify that the required entries in the .env file exist (and are not set to empty strings)
for i in "${!REQUIRED_ENV_VARS[@]}"
do
    :
    if ! grep -qoEx "${i}=.+" "${ENV_FILE}"; then
        printf "${RED}Error:${NC} Missing required line from your .env file: \"$i={your_value}\"\n"
        ERRORS_EXIST=true
    else
        REQUIRED_ENV_VARS[$i]=`grep -oEx "${i}=.+" "${ENV_FILE}" | sed -n -e "s/^.*${i}=//p"`
    fi
done

# Validate env variable values
for i in "${!REQUIRED_ENV_VARS[@]}"
do
    :
    if [ ${i} == "MYSQL_DATABASE" ]; then
        #echo "Checking MYSQL_DATABASE length..."
        if [ ${#REQUIRED_ENV_VARS[$i]} -gt ${MAX_MYSQL_DATABASE_NAME_LENGTH} ]; then
            printf "${RED}Error:${NC} The length of MYSQL_DATABASE set in file \"${ENV_FILE}\" must not exceed ${MAX_MYSQL_DATABASE_NAME_LENGTH} characters in length.\n"
            ERRORS_EXIST=true
        fi
    elif [ ${i} == "MYSQL_USER" ]; then
        #echo "Checking MYSQL_USER length..."
        if [ ${#REQUIRED_ENV_VARS[$i]} -gt ${MAX_MYSQL_USERNAME_LENGTH} ]; then
            printf "${RED}Error:${NC} The length of MYSQL_USER set in file \"${ENV_FILE}\" must not exceed ${MAX_MYSQL_USERNAME_LENGTH} characters in length.\n"
            ERRORS_EXIST=true
        fi
    fi
done

if [ ${ERRORS_EXIST} == true ]; then
    exit 0;
fi


# Verify MySQL admin credentials.
# The user will be re-prompted to enter correct details if the default credentials fail, or if any subsequent attempt fails.
MYSQL_HOST="${REQUIRED_ENV_VARS[MYSQL_HOST]}"
MYSQL_DATABASE="${REQUIRED_ENV_VARS[MYSQL_DATABASE]}"
MYSQL_ADMIN_USERNAME="${REQUIRED_ENV_VARS[MYSQL_ADMIN_USERNAME]}"
MYSQL_ROOT_PASSWORD="${REQUIRED_ENV_VARS[MYSQL_ROOT_PASSWORD]}"


if [[ -z "${MYSQL_HOST}" || -z "${MYSQL_ADMIN_USERNAME}" || -z "${MYSQL_ROOT_PASSWORD}" ]]; then
    get_mysql_credentials
fi

while ! mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p"${MYSQL_ROOT_PASSWORD}" -e ";" >/dev/null 2>&1; do
    echo ""
    echo "*** Your default MySQL admin credentials failed verification (or unable to connect to the default host name). ***"
    echo ""

    get_mysql_credentials
done


# Create the database
#mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "DROP DATABASE IF EXISTS \`${REQUIRED_ENV_VARS[MYSQL_DATABASE]}\`;" >/dev/null 2>&1;
mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS \`${REQUIRED_ENV_VARS[MYSQL_DATABASE]}\`;" >/dev/null 2>&1

# Verify MySQL database was created (mysql exit code 0)
mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "use \`${REQUIRED_ENV_VARS[MYSQL_DATABASE]}\`;" >/dev/null 2>&1
if [ $? == "0" ]; then
    printf "Created MySQL database ${GREEN}${REQUIRED_ENV_VARS[MYSQL_DATABASE]}${NC}.\n"
    printf "${GREEN}Database verified!${NC} -- Verified database ${GREEN}${REQUIRED_ENV_VARS[MYSQL_DATABASE]}${NC} exists on host ${GREEN}${REQUIRED_ENV_VARS[MYSQL_HOST]}${NC}\n"
else
    printf "${RED}Warning: Database could not be verified!${NC} -- Unable to verify that database ${RED}${REQUIRED_ENV_VARS[MYSQL_DATABASE]}${NC} was created on host ${RED}${REQUIRED_ENV_VARS[MYSQL_HOST]}${NC}\n"
fi

# Create the MySQL user
#mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "DROP USER '${REQUIRED_ENV_VARS[MYSQL_USER]}'@'${REQUIRED_ENV_VARS[MYSQL_HOST]}';" >/dev/null 2>&1;
mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "CREATE USER '${REQUIRED_ENV_VARS[MYSQL_USER]}'@'${REQUIRED_ENV_VARS[MYSQL_HOST]}' IDENTIFIED BY '${REQUIRED_ENV_VARS[MYSQL_PASSWORD]}';" >/dev/null 2>&1;
if [ $? == "0" ]; then
    printf "Created MySQL user ${GREEN}${REQUIRED_ENV_VARS[MYSQL_USER]}${NC} with password ${GREEN}${REQUIRED_ENV_VARS[MYSQL_PASSWORD]}${NC}.\n"
else
    printf "${RED}Warning: User could not be verified!${NC} -- Unable to verify if ${RED}${REQUIRED_ENV_VARS[MYSQL_USER]}${NC} was created on database ${RED}${REQUIRED_ENV_VARS[MYSQL_DATABASE]}${NC} on host ${RED}${REQUIRED_ENV_VARS[MYSQL_HOST]}${NC}\n"
fi
mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON ${REQUIRED_ENV_VARS[MYSQL_DATABASE]}.* TO '${REQUIRED_ENV_VARS[MYSQL_USER]}'@'${REQUIRED_ENV_VARS[MYSQL_HOST]}';"  >/dev/null 2>&1
if [ $? == "0" ]; then
    printf "${GREEN}Privileges granted!${NC} -- Verified user ${GREEN}${REQUIRED_ENV_VARS[MYSQL_USER]}${NC} was created on database ${GREEN}${REQUIRED_ENV_VARS[MYSQL_DATABASE]}${NC} on host ${GREEN}${REQUIRED_ENV_VARS[MYSQL_HOST]}${NC}\n"
else
    printf "${RED}Warning: User could not be verified!${NC} -- Unable to verify if privileges was granted on database ${RED}${REQUIRED_ENV_VARS[MYSQL_DATABASE]}${NC} on host ${RED}${REQUIRED_ENV_VARS[MYSQL_HOST]}${NC}\n"
fi
mysql -h "${MYSQL_HOST}" -u "${MYSQL_ADMIN_USERNAME}" -p${MYSQL_ROOT_PASSWORD} -e "FLUSH PRIVILEGES;" >/dev/null 2>&1


##### BDU Installation

if [[ -z "$BDU_DUMP_FILE" || ! -f "${BDU_DUMP_FILE}" ]]; then
    printf "${RED}Error:${NC} Dump file not exists\n"
else
    mysql -h "${MYSQL_HOST}" -u "${REQUIRED_ENV_VARS[MYSQL_USER]}" -p${REQUIRED_ENV_VARS[MYSQL_PASSWORD]} ${REQUIRED_ENV_VARS[MYSQL_DATABASE]} < ${BDU_DUMP_FILE}  >/dev/null 2>&1
    printf "${GREEN}BDU creation done!${NC}\n"
fi
