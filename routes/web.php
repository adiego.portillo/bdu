<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// IMPORTANTE //

/*
    Debe viajar el HEADER Accept application/json para que las respuestas con errores sean manejadas correctamente,
    de lo contrario retorna un html con mensaje del error
*/

Route::get('/', function () {
    return view('welcome');
});
//logout
// Rutas publicas
Route::post('/v1/login', [AdminController::class,'login']);

// Rutas que solo requieren autenticacion.
Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/v1/logout', [AdminController::class,'logout']); 
    Route::put('/v1/perfil/password',[AdminController::class,'cambiarPassword']);
    Route::get('/v1/perfil/listado-rutas', [AdminController::class,'listarRutasUsuario']);
    Route::get('/v1/perfil/obtenerMisDatos', [AdminController::class,'obtenerMisDatos']);
    Route::put('/v1/perfil', [AdminController::class,'actualizarMisDatos']);

    
    
});
// can:admin-users es un Gate definido en AuthServiceProvider, verifica si el usuario pertenece al grupo Administradores

// Rutas autenticadas que pertenezcan al grupo de Administradores
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/v1/admin/usuarios', [AdminController::class,'listarUsuarios'])->middleware("can:acceso-ruta");
    Route::post('/v1/admin/usuarios',[AdminController::class,'creaUsuario'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/usuarios/{id}',[AdminController::class,'actualizaUsuario'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/usuarios/{id}',[AdminController::class,'borrarUsuario'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/{id}',[AdminController::class,'obtenerUsuarioId'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/email/{email}',[AdminController::class,'obtenerUsuarioEmail'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/usuario/{name}',[AdminController::class,'obtenerUsuarioNombre'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/apellido/{apellido}',[AdminController::class,'obtenerUsuarioApellido'])->middleware("can:acceso-ruta");

    Route::put('/v1/admin/cambiarPasswordUsers', [AdminController::class,'cambiarPasswordUsers'])->middleware("can:acceso-ruta");

    Route::post('/v1/admin/grupos',[AdminController::class,'crearGrupo'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/grupos/{id}',[AdminController::class,'actualizarGrupo'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/grupos/{id}',[AdminController::class,'borrarGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos', [AdminController::class,'listarGrupos'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/{id}', [AdminController::class,'obtenerGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/nombre/{nombre}', [AdminController::class,'obtenerGrupoNombre'])->middleware("can:acceso-ruta");

    Route::post('/v1/admin/rutas',[AdminController::class,'crearRuta'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/rutas/{id}',[AdminController::class,'actualizarRuta'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/rutas/{id}',[AdminController::class,'borrarRuta'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/rutas', [AdminController::class,'listarRutas'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/rutas/{id}', [AdminController::class,'obtenerRuta'])->middleware("can:acceso-ruta");

    Route::delete('/v1/admin/rutasmetodo/{id}',[AdminController::class,'borrarRutaMetodo'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/rutasmetodo',[AdminController::class,'borrarRutaMetodo1'])->middleware("can:acceso-ruta");
    
    Route::post('/v1/admin/usuarios-grupos',[AdminController::class,'crearUsuarioGrupo'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/usuarios-grupos',[AdminController::class,'eliminarUsuarioGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios-grupos',[AdminController::class,'obtenerUsuariosGrupos'])->middleware("can:acceso-ruta");
    
    Route::get('/v1/admin/usuarios-grupos/usuario/{id}',[AdminController::class,'obtenerGruposDeUsuario'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios-grupos/grupo/{id}',[AdminController::class,'obtenerUsuariosDeGrupo'])->middleware("can:acceso-ruta");

    Route::post('/v1/admin/grupos-rutas',[AdminController::class,'crearGrupoRuta'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/grupos-rutas/{id}',[AdminController::class,'eliminarGrupoRuta'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos-rutas',[AdminController::class,'obtenerGruposRutas'])->middleware("can:acceso-ruta");
    
    Route::get('/v1/admin/grupos-rutas/grupo/{id}',[AdminController::class,'obtenerRutasDeGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos-rutas/ruta/{id}',[AdminController::class,'obtenerGruposDeRuta'])->middleware("can:acceso-ruta");


});

//BD Proyecto2021
//Las funcines de rutas a una base de datos externas, deben colocarse en ApiController
Route::middleware(['auth:sanctum'])->group(function () {


    Route::get('/v2/prueba/administrativos', [ApiController::class,'listarAdministrativos'])->middleware("can:acceso-ruta");

});
