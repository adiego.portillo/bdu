# Base de Datos Unificada
## Contenido
- [Instalación como Contenedor Docker](#instalación-como-contenedor-docker)
- [Instalación Manual](#instalación-manual)
- [Resolución de Problemas](#resolución-de-problemas)
- [Documentación de Desarrollador](#documentación-de-desarrollador)
- [Documentación de Usuario](#documentación-de-usuario)


# Instalación como Contenedor Docker
A continuación se enumeran los pasos para la instalación del software BDU como contenedor Docker.

## Requerimientos
- Sistema Operativo GNU/Linux
- Git
- Docker
- Docker Compose

## Descarga del proyecto
```bash
$ cd ~
$ git clone https://gitlab.com/modernizacion.sc/bdu.git
```
## Selección de tipo de despliegue
### Opción 1. Contenedor Nginx como Proxy Reverso + Contenedor Apache-PHP-BDU + Contenedor MySQL + Contenedor PHPMyAdmin
- Utilizar el archivo `docker-compose.yml` provisto por defecto

### Opción 2. Contenedor Apache-PHP-BDU + Contenedor MySQL
- Utilizar el archivo `docker-compose.yml` opcional:
```bash
$ cd ~/bdu
$ cp docker-compose.yml.opc docker-compose.yml
```
- Editar el archivo `~bdu/nginx/nginx.conf` y definir la variable `server_name` correspondiente a su dominio.

## Configuración de variables de entorno
```bash
$ cd ~/bdu
$ cp .env-docker.example .env
```
- Editar el archivo `~/bdu/.env` y ajustar, de ser necesario, las siguientes variables:
	- Datos de acceso de administrador (necesarios para la creación de la base de datos y usuario):
		- `MYSQL_ADMIN_USERNAME`
		- `MYSQL_ROOT_PASSWORD`
	- Usuario de base de datos BDU:
		- `MYSQL_USER`
		- `MYSQL_PASSWORD`
	- Datos de acceso a la base de datos local que interactuará con el API:
		- `LOCAL_DB_HOST`
		- `LOCAL_DB_PORT`
		- `LOCAL_DB_DATABASE`
		- `LOCAL_DB_USERNAME`
		- `LOCAL_DB_PASSWORD`

## Creación del contenedor
```bash
$ sudo docker-compose up -d
```
## Permisos en carpeta storage
```bash
$ sudo chmod -R 775 ~/bdu/storage 
```

# Instalación Manual
A continuación se enumeran los pasos para la instalación del software BDU bajo sistema operativo GNU/Linux

## Requerimientos
- Sistema Operativo GNU/Linux (Verificado para Debian 9 y 10)
- Servidor Web Apache
- PHP 7.4+
- Mysql 8+ o MariaDB 10.1+
- Composer 2.0+

## Preparación e instalación de paquetes varios:
```bash
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install git nano wget curl software-properties-common gnupg2
```
## Instalación de MySQL 8
```bash
$ wget https://dev.mysql.com/get/mysql-apt-config_0.8.16-1_all.deb
$ sudo dpkg –i mysql-apt-config_0.8.16-1_all.deb
$ sudo apt update
$ sudo apt install mysql-server
	Contraseña: A definir por el administrador.
	Use Strong Password Encryption
$ sudo systemctl enable mysql
```
## Instalación de Servidor Web Apache y PHP 7.4
```bash
$ sudo apt install apt-transport-https lsb-release ca-certificates
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
$ sudo apt update
$ sudo apt install apache2 php7.4 libapache2-mod-php7.4 php7.4-{mysql,xml,bcmath,bz2,intl,gd,mbstring,zip}
```
## Instalación de Composer
```bash
$ sudo wget -O composer-setup.php https://getcomposer.org/installer
$ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```
## Descarga e instalación del Software BDU
```bash
$ cd ~
$ git clone https://gitlab.com/modernizacion.sc/bdu.git
$ cd bdu
$ cp .env.example .env
$ composer install --no-dev
$ php artisan key:generate
$ sudo chown –R www-data:www-data ~/bdu
$ sudo chmod –R 775 ~/bdu/storage
```
## Creación de Base de Datos, usuario y migración de Tablas MySQL
- Editar el archivo `~/bdu/.env` y ajustar las siguientes variables:
	- Datos de acceso de administrador (necesarios para la creación de la base de datos y usuario):
		- `MYSQL_ADMIN_USERNAME`
		- `MYSQL_ROOT_PASSWORD`
	- Usuario de base de datos BDU:
		- `MYSQL_USER`
		- `MYSQL_PASSWORD`
	- Datos de acceso a la base de datos local que interactuará con el API:
		- `LOCAL_DB_HOST`
		- `LOCAL_DB_PORT`
		- `LOCAL_DB_DATABASE`
		- `LOCAL_DB_USERNAME`
		- `LOCAL_DB_PASSWORD`
```bash
$ cd ~/bdu
$ sudo chmod +x ./setup_mysql.sh
$ ./setup_mysql.sh
```
## Configuración de Virtual Host Apache
```bash
$ sudo cp ~/bdu/apache/sites-available/bdu.conf /etc/apache2/sites-available
$ sudo cp ~/bdu/apache/sites-available/bdu-ssl.conf /etc/apache2/sites-available 
$ sudo rm /etc/apache2/sites-enabled/*
$ sudo ln –s /etc/apache2/sites-available/bdu.conf /etc/apache2/sites-enabled/
$ sudo ln –s /etc/apache2/sites-available/bdu-ssl.conf /etc/apache2/sites-enabled/
```
- Modificar en los archivos `/etc/apache2/sites-available/bdu.conf` y `/etc/apache2/sites-available/bdu-ssl.conf` las rutas de las variables `DocumentRoot` y `Directory` por la correspondiente al directorio de instalación de BDU.
```bash
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2
```
# Resolución de Problemas
## Desinstalación de software de base de datos existente (MySQL o MariaDB)
- En caso de contar con una versión previa de MySQL o MariaDB instalada en el sistema, se podrá desinstalar siguiendo los siguientes pasos:
```bash
$ sudo apt remove --purge mysql-\*
$ sudo apt remove --purge mariadb-\*
$ sudo apt --fix-broken install
$ sudo apt autoremove
```
## Creación manual de base de datos BDU
- En caso de requerirlo, se dispone de los scripts correspondientes a la creación de la base de datos, usuario de administración, tablas y datos iniciales en `~/bdu/mysql/extras` y `~/bdu/mysql/initdb.d`
## Desinstalación de software PHP existente
- En caso de contar con la versión 5 de PHP, es posible desinstalarlo mediante el siguiente comando:
```bash
$ sudo apt remove libapache2-mod-php5 php5
```
- En caso de contar con la versión 7.3 de PHP, es posible desinstalarlo mediante el siguiente comando:
```bash
$ sudo apt remove libapache2-mod-php7.3 php7.3
```
## Reconstrucción de los Contenedores Docker
### Contenedor MySQL 
Para regenerar el proyecto desde cero, incluyendo la creación de tablas de MySQL, se deberá eliminar manualmente el contenido del directorio `bdu/mysql/data`:
```bash
$ cd ~
$ sudo rm -rf bdu/mysql/data/*
```
- Reconstruir el contenedor:
```bash
$ cd ~/bdu
$ docker-compose buid bdu_bd
```
### Contenedor Servidor Web Apache, PHP y proyecto
- Reconstruir el contenedor:
```bash
$ cd ~/bdu
$ docker-compose buid bdu
```

# Documentación de Desarrollador
## Configuración de bases de datos adicionales
- Para generar conexiones a bases de datos adicionales se deberán incluir un apartado para el controlador correspondiente dentro del arreglo `connections` del archivo `bdu/config/database.php`.
- Los datos de conexión (host, puerto, usuario, contraseña) se establecerán en el archivo `bdu/.env`.

## Definición de Rutas
Las rutas disponibles para el API se deben definir en `bdu/routes/web.php`. Se han definido cuatro secciones:
- Rutas públicas, accesibles por todos los usuarios sin requerir autenticación.
- Rutas para todos los usuarios autenticados.
- Rutas para usuarios administradores. Aquí se definen por defecto las rutas correspondientes a tareas de administración del API. 
- Rutas locales, donde se sefinirán las rutas específicas de los recursos compartidos desde el API.

Para detalles de utilización del API ver la [Documentación de Usuario](#documentación-de-usuario)

## Configuración de Controladores
Toda la lógica de programación de las rutas se define a través de controladores. 
- Las funciones específicas relacionadas a las rutas administrativas se encuentran en `bdu/app/Http/Controllers/AdminController.php`.
- Las funciones correspondientes al API se establecen en `bdu/app/Http/Controllers/ApiController.php`.

## Repositorio de Conexiones a Bases de Datos
- La conexión a la base de datos BDU y consultas SQL administrativas se establecen `bdu/app/Repository/AdminRepository`.
- Las conexiones y consultas a las bases de datos locales se establecen en `bdu/app/Repository/ApiRepository`.

# Documentación de Usuario

## Colección Postman
Postman es una herramienta que permite crear y probar peticiones sobre APIs de forma sencilla. Puede obtenerse gratuitamente desde este enlace:
- [Descargar la aplicación POSTMAN](https://www.postman.com/downloads/)

Las rutas disponibles por defecto en BDU se encuentran agrupadas en una colección POSTMAN:
- [Descargar colección BDU para POSTMAN](BDU.postman_collection.json)

> Dentro de Postman definir las variables `{{HOST}}` y `{{TOKEN}}` para la correcta utilización de la colección.

## Perfil Administrador
El sistema cuenta con un usuario por defecto con privilegios administrativos:
- Usuario: **admin**
- Grupo: **Administradores**
- Contraseña inicial: **admin**

## Rutas Públicas
| Descripción | Método | Ruta |
| :----------- | :------: | :---- |
| Login | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/login

## Rutas para usuarios autenticados
| Descripción | Método | Ruta |
| :----------- | :------: | :---- |
| Cambio de contraseña | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/perfil/password |
| Listar rutas accesibles por usuario | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/perfil/listado-rutas |
| Obtener mis datos | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/perfil/obtenerMisDatos |
| Modificar mis datos | ![PUT](https://img.shields.io/badge/%20-PUT-yellow) | /v1/perfil |
| Logout | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/logout |
## Rutas restringidas a Administradores
| Descripción | Método | Ruta |
| :----------- | :------: | :---- |
| Crear un usuario |![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/admin/usuarios |
| Modificar un usuario | ![PUT](https://img.shields.io/badge/%20-PUT-yellow) | /v1/admin/usuarios/{id_usuario} |
| Modificar contraseña de un usuario | ![PUT](https://img.shields.io/badge/%20-PUT-yellow) | /v1/admin/cambiarPasswordUsers |
| Eliminar un usuario | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/usuarios/{id_usuario} |
| Obtener usuarios | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios |
| Obtener un usuario | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios/{id_usuario} |
| Obtener un usuario por email| ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios/email/{email} |
| Obtener un usuario por nombre| ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios/usuario/{name} |
| Obtener un usuario por apellido| ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios/apellido/{apellido} |
| Crear un grupo | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/admin/grupos |
| Modificar un grupo | ![PUT](https://img.shields.io/badge/%20-PUT-yellow) | /v1/admin/grupos/{id_grupo} |
| Eliminar un grupo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/grupos/{id_grupo} |
| Obtener grupos | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/grupos |
| Obtener un grupo | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/grupos/{id_grupo} |
| Obtener un grupo por su nombre | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/grupos/nombre/{nombre} |
| Agregar un usuario a un grupo | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/admin/usuarios-grupos |
| Eliminar un usuario de un grupo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/usuarios-grupos/{id_usuario}&{id_grupo} |
| Obtener todos los usuarios y grupos | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios-grupos |
| Obtener los usuarios de un grupo | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios-grupos/grupo/{id_grupo} |
| Obtener los grupos de un usuario | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/usuarios-grupos/usuario/{id_usuario} |
| Crear una ruta | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/admin/rutas |
| Modificar una ruta | ![PUT](https://img.shields.io/badge/%20-PUT-yellow) | /v1/admin/rutas/{id_ruta} |
| Eliminar una ruta | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/rutas/{id_ruta} |
| Eliminar una ruta por metodo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/rutasmetodo/{id_grupo}&{id_ruta}&{metodo_acceso} |
| Eliminar una ruta por metodo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/rutasmetodo/{id_gruporutametodo} |
| Obtener rutas | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/rutas |
| Obtener una ruta | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/rutas/{id} |
| Agregar una ruta a un grupo | ![POST](https://img.shields.io/badge/%20-POST-brightgreen) | /v1/admin/grupos-rutas |
| Eliminar una ruta de un grupo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/grupos-rutas/{id_gruporutasmetodos} |
| Obtener todos los grupos con rutas | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/grupos-rutas |
| Obtener todas las rutas de un grupo | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/grupos-rutas/grupo/{id_grupo} |
| Obtener todos los grupos de una ruta | ![GET](https://img.shields.io/badge/%20-GET-blue) | /v1/admin/grupos-rutas/ruta/{id_ruta} |
 
