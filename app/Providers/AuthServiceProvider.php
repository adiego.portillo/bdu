<?php

namespace App\Providers;

use App\Repository\AdminRepository;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Determina si un usuario pertenece al grupo de Administradores.
        // Agregar otros Gates similiares si hay que verificar por otros grupos
        Gate::define('admin-users', function ($user) {
            Log::info("Verificando Usuario Admin: ".$user->email);
            $result = AdminRepository::perteneceGrupo($user->id,"Administradores");
            return sizeof($result) > 0;
        });

        /**
         * Determina si un usuario tiene acceso a la ruta solicitada en el request
         */
        Gate::define('acceso-ruta', function ($user) {
            Log::info("Verificando acceso a ruta a usuario: ".$user->email);
            $request = resolve(\Illuminate\Http\Request::class);
            //$ruta = substr($request->path(),6);
            $ruta = "/".$request->path();
            $method = strtoupper($request->method());
            Log::info("Ruta: ".$ruta);
            Log::info("Metodo: ".$method);
            // Llamar a repository que consulta por email o id, si el usuario pertenece a un grupo determinado
            $result = AdminRepository::accesoRuta($user->id,$ruta,$method);
            Log::debug("Acceso a ruta DB: ".print_r($result,true));
            return sizeof($result) > 0;
        });

    }

}
