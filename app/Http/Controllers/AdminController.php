<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repository\AdminRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    /*
     **************************** USERS ******************************
     */



    /**
     * Registra un nuevo usuario en la base de datos
     */
    public function registrarUsuario($datos)
    {
        return "";
    }


    /**
     * Realiza el login del usuario.
     * Si el usuario existe crea un token y lo retorna.
     */
    public function login(Request $request)
    {
        $user = User::where('usuario', $request->usuario)->first();
        Log::info('Intento de login usuario: ' . $request->usuario);

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json("Credenciales inválidas", 403);
        }

        // Obtiene un token y se almacena automaticamente en la tabla de tokens asignados a cada usuario
        return response()->json(["access_token" => $user->createToken($request->usuario)->plainTextToken]);
    }


    //Realiza el logout del usuario
    public function logout(Request $request)
    {
        $user=$request->user();
        $user->tokens()->delete();
        return response()->json("Su sesion a finalizado");

    }


    /**
     * Retorna todos los usuarios registrados
     */
    public function listarUsuarios()
    {
        try{
            
            $usuarios = User::all();
            
            if($usuarios != '[]'){
            return response()->json($usuarios);
            }
            else{
                return response()->json("No existen usuarios cargados.", 404);
            }

        } 
        catch (Exception $e) {

            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Permite al usuario actual logueado (sesion actual) actualizar su password
     * Recibe en el body del request los datos del usuario.
     * passwordAnterior, nuevaPassword, confirmacionNuevaPassword
     */
    public function cambiarPassword(Request $request)
    {
        $usuario = $request->user();
        Log::info("Actualizar password de " . $usuario->usuario);
        $datos = $request->all();
        if( Hash::check($datos["password"], $usuario->password)){
            if(AdminController::validar_clave($datos["nuevo_password"], $error_clave)){

                $nuevo_password = Hash::make($datos["nuevo_password"]);
                $usuario->password = $nuevo_password;
                $usuario->save();
                return response()->json("Contraseña de ".$usuario->usuario." actualizada", 201);
        
            }
            else
            {
                return response()->json($error_clave, 404);
            }
        
        }
        else {
            return response()->json("No se pudo actualizar la contraseña de ".$usuario->usuario.". Contraseña incorrecta.", 400);
        }
        
    }


    //Obtiene los datos del usuario actual logueado (sesion actual)
    public function obtenerMisDatos(Request $request)
    {
        try {
            
            $usuario = $request->user();
            
            $usuario = User::find($usuario->id);

            return response()->json($usuario);

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    //Permite al usuario Administrador actualizar el password de otro usuario.
    public function cambiarPasswordUsers(Request $request)
    {
        $datos = $request->password;
        $usuario = User::find($request->id);
        if(AdminController::validar_clave($datos["password"], $error_clave)){
        Log::info("Actualizar password de " . $usuario->usuario);
        
            $nuevo_password = Hash::make($datos["password"]);
            $usuario->password = $nuevo_password;
            $usuario->save();
            return response()->json("Contraseña de ".$usuario->usuario." actualizada", 201);
            }
            else{
                return response()->json($error_clave, 404);
            }
    }


    /**
     * Da de alta un usuario.
     * Recibe en el body del request los datos del usuario a crear.
     */
    public function creaUsuario(Request $request)
    {
        try {
            $datos = $request->all();
            
            Log::debug("Crear usuario: " . print_r($datos, true));
        
            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();

            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();
            
            //Validamos, si el email no existe en la BD, validamos el nombre de usuario. 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(empty($emailUsuario)){
                if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                    //si el nombre de usuario tampoco existe en la BD, se crea el usuario ya que ninguno
                    //de los campos unique se repiten.
                    if(empty($nombreUsuario)){
                        if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]) && preg_match("/^[a-zA-Z ]*$/",$datos["name"] ))
                        {
                            if(AdminController::validar_clave($datos["password"], $error_clave)){
                            // codifica la password
                            $datos["password"] = Hash::make($datos["password"]);
                            $usuario = new User();
                            $usuario->create($datos);
                            return response()->json("Usuario creado con éxito");
                            
                            }
                        else{
                            return response()->json($error_clave, 404);
                        }
                    }
                        else{
                            return response()->json("Solo letras o espacios en blanco para Nombre y Apellido", 404);
                        }
                    }
                    else{
                        return response()->json("El nombre de usuario ya existe en la BD", 404);
                    }
                }
                else{
                    return response()->json("Formato de email incorrecto", 404);
                }
            }
            else{
                return response()->json("El email ya existe en la BD", 404);
            }
        
        }
        //Captura la excepcion lanzada al intentar dar de alta un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    //Permite actualizar los datos del usuario actual logueado (sesion actual)
    public function actualizarMisDatos(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
        
            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();
                        
            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();

            //Validamos, si el email no existe en la BD o el email actual es igual al que se encuentra en el body
            //(esto quiere decir que no lo quiero modificar) 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(empty($emailUsuario) || $usuario->email == $datos["email"]){
                if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                    //si el nombre de usuario tampoco existe en la BD o el usuario actual es igual al que se encuentra en el body, 
                    //(esto quiere decir que no lo quiero modificar), se modifica el usuario ya que ninguno de los campos unique se repiten.
                    if(empty($nombreUsuario) || $usuario->usuario == $datos["usuario"]){
                        if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]) && preg_match("/^[a-zA-Z ]*$/",$datos["name"] ))
                        {
                            // Se pisan todos los datos del usuario.
                            // Si el dato no cambio, debe venir igual con el valor previo. De lo contrario hay que verificar si el dato
                            // exista, realizar la asignacion.
                            Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                            $usuario->name = $datos["name"];
                            $usuario->apellido = $datos["apellido"];
                            $usuario->usuario = $datos["usuario"];
                            $usuario->descripcion = $datos["descripcion"];
                            $usuario->email = $datos["email"];
                            $usuario->save();
                            return response()->json("Usuario modificado con éxito", 201);
                        }
                        else{
                            return response()->json("Solo letras o espacios en blanco para Nombre y Apellido", 404);
                        }
                    }
                    else{
                        return response()->json("El nombre de usuario ya existe en la BD", 404);
                    }
            }
            else{
                return response()->json("Formato de email incorrecto", 404);

            }
        }
        else{
            return response()->json("El email ya existe en la BD", 404);
        }
            
        } 
        //Captura la excepcion lanzada al intentar modificar un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD, distinto al que posee.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Permite al usuario Administrador actualizar todos los datos de un usuario.
     */
    public function actualizaUsuario(Request $request, $id)
    {
        try {
            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);

            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();
                        
            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

            //Validamos, si el email no existe en la BD o el email actual es igual al que se encuentra en el body
            //(esto quiere decir que no lo quiero modificar) 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(empty($emailUsuario) || $usuario->email == $datos["email"]){
                    if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                //si el nombre de usuario tampoco existe en la BD o el usuario actual es igual al que se encuentra en el body, 
                //(esto quiere decir que no lo quiero modificar), se modifica el usuario ya que ninguno de los campos unique se repiten.
                        if(empty($nombreUsuario) || $usuario->usuario == $datos["usuario"]){
                            if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]) && preg_match("/^[a-zA-Z ]*$/",$datos["name"] ))
                            {
                                // No se actualiza el password
                                // Se pisan todos los datos del usuario.
                                // Si el dato no cambio, debe venir igual con el valor previo. De lo contrario hay que verificar si el dato
                                // exista, realizar la asignacion.
                                Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                                $usuario->name = $datos["name"];
                                $usuario->apellido = $datos["apellido"];
                                $usuario->usuario = $datos["usuario"];
                                $usuario->descripcion = $datos["descripcion"];
                                $usuario->email = $datos["email"];
                                $usuario->save();
                                return response()->json("Usuario modificado con éxito", 201);
                            }
                            else{
                                return response()->json("Solo letras o espacios en blanco para Nombre y Apellido", 404);
                            }
                        
                        }
                        else{
                            return response()->json("El nombre de usuario ya existe en la BD", 404);
                        }
                    }
                    else{
                        return response()->json("Formato de email incorrecto", 404);
                    }
                }
                else{
                    return response()->json("El email ya existe en la BD", 404);
                }
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
        //Captura la excepcion lanzada al intentar modificar un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD, distinto al que posee.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    /**
     * Elimina un usuario y sus asociaciones (grupos a los que pertenece)
     */
    public function borrarUsuario($id)
    {
        try {

            $usuario = User::find($id);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){
                // Borra las asociaciones (grupos a los que pertenece) que existan con ese usuario
                AdminRepository::borrarUsuarioGrupo($id);
                //Borra al usuario
                $usuario->delete();
                return response()->json("Usuario borrado");
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Retorna los datos de un usuario, sin asociaciones, por Id
     */
    public function obtenerUsuarioId($id)
    {
        try {
            $usuario = User::find($id);

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Retorna los datos de un usuario, por email
     */
    public function obtenerUsuarioEmail($email)
    {
        try {
            $usuario = User::where('email', '=', $email)->first();
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el email: ".$email, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Retorna los datos de los usuarios, por nombre
     */
    public function obtenerUsuarioNombre($name)
    {
        try {
            $usuario = User::where('name', '=', $name)->get();
            
            if($usuario != '[]'){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el nombre: ".$name, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Retorna los datos de un usuario, por apellido
     */
    public function obtenerUsuarioApellido($apellido)
    {
        try {
            $usuario = User::where('apellido', '=', $apellido)->get();

            if($usuario != '[]'){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el apellido: ".$apellido, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    

    /*
     **************************** GRUPOS ******************************
     */


    /**
     * Crea un nuevo grupo
     */
    public function crearGrupo(Request $request)
    {
        try {
            
            $datos = $request->all();

            //Consultamos en la BD si el nombre del grupo que introducimos en el body ya existe
            $nombreGrupo = AdminRepository::obtenerGrupoNombre($datos["nombre"]);

            if(empty($nombreGrupo)){

                Log::debug("Creando grupo: " . print_r($datos, true));
                AdminRepository::crearGrupo($datos);
                return response()->json("Grupo creado con éxito");
            }
            else{
                return response()->json("El nombre del grupo que intenta crear ya existe", 404);
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Actualiza los datos de un grupo.
     */
    public function actualizarGrupo(Request $request, $id)
    {
        try {
            $datos = $request->all();
            Log::debug("Actualizando grupo: " . print_r($datos, true));

            //Consultamos en la BD si el ID del grupo que queremos modificar existe
            $grupo = AdminRepository::obtenerGrupo($id);
        
            //Consultamos en la BD si el nombre del grupo que introducimos en el body ya existe en cualquier otro grupo, distinto al que queremos modificar.
            $nombre = AdminRepository::obtenerGrupoNombre($datos["nombre_grupo"]);
            
            //Si $grupo no esta vacio, quiere decir que el grupo existe.
            if(!empty($grupo)){
                
                //Si el nombre no se encuentra en la BD o el nombre que enviamos en el body es igual al que ya tiene el grupo a modificar, actualizamos.
                if(empty($nombre) || $datos["nombre_grupo"] == $grupo[0]->nombre_grupo){

                    AdminRepository::actualizarGrupo($datos, $id);
                    return response()->json("Grupo actualizado", 201);    
                }
                else{
                    return response()->json("El nombre del grupo ya existe.");    
                }
            }
            else{
                return response()->json("El grupo con el Id: " . $id . " no existe.");
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    /**
     * Elimina un grupo.
     * TODO verificar si tiene registros de otras tablas vinculados, o si fallaría por integridad referencial
     */
    public function borrarGrupo($idGrupo)
    {
        try {

            $grupo = AdminRepository::obtenerGrupo($idGrupo);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($grupo)){
            
                $resultado = AdminRepository::borrarGrupo($idGrupo);

                if($resultado == 1){
                    return response()->json("Grupo borrado");
                }else{
                    return response()->json("No se ha podido borrar el grupo", 404);
                }

            }
            else{
                return response()->json("No existe ningun grupo con el id: ".$idGrupo, 404);
            }

        } 
        //Captura la excepcion lanzada al intentar borrar un grupo que tenga usuarios y/o rutas asociados.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El grupo que desea eliminar tiene usuarios y/o rutas asociados.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    /**
     * Lista todos los grupos
     */
    public function listarGrupos()
    {
        try {
            return AdminRepository::listarGrupos();
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    /**
     * Obtiene un grupo determinar
     */
    public function obtenerGrupo($id)
    {
        try {
            $grupo = AdminRepository::obtenerGrupo($id);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($grupo)){
                return response()->json($grupo);
            }
            else{
                return response()->json("No existe ningun grupo con el id: ".$id, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    public function obtenerGrupoNombre($nombreGrupo)
    {
        try {
            $grupo = AdminRepository::obtenerGrupoNombre($nombreGrupo);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($grupo)){
                return response()->json($grupo);
            }
            else{
                return response()->json("No existe ningun grupo con el nombre: ".$nombreGrupo, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

  

            
    /*
     **************************** RUTAS ******************************
     */


    //Crea una ruta
    public function crearRuta(Request $request)
    {
            
        try {
            $datos = $request->all();

            Log::debug("Creando ruta: " . print_r($datos, true));
            AdminRepository::crearRuta($datos);
            return response()->json("Ruta creada con éxito");
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    
    /**
     * Actualiza los datos de una ruta
     */
    public function actualizarRuta(Request $request, $id)
    {
        try {

            $ruta = AdminRepository::obtenerRuta($id);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($ruta)){

                $datos = $request->all();

                Log::debug("Actualizando ruta: " . print_r($datos, true));

                $resultado = AdminRepository::actualizarRuta($datos, $id);

                if($resultado == 1){

                    return response()->json("Ruta actualizada", 201);

                }
                else{
                    return response()->json("No se ha podido actualizar la ruta", 404);
                }

            }
            else{
                return response()->json("No existe ninguna ruta con el id: ".$id, 404);
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Elimina un ruta por metodo de la tabla grupos_rutas_metodos, segun la PK de la tabla.
     */
    public function borrarRutaMetodo($idRuta)
    {
        try {
            $resultado = AdminRepository::borrarRutaMetodo($idRuta);
            
            if($resultado == 1){
                return response()->json("ruta borrada");
            }else{
                return response()->json("No se ha podido borrar la ruta", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Elimina un ruta por metodo de la tabla grupos_rutas_metodos, segun el id_grupo, id_ruta y el metodo_acceso.
     */
    public function borrarRutaMetodo1(REQUEST $request)
    {
        try {
            
            $idGrupo = $request->id_grupo;
            $idRuta = $request->id_ruta;
            $metodoAcceso = $request->metodo_acceso;

            $resultado = AdminRepository::borrarRutaMetodo1($idGrupo, $idRuta, $metodoAcceso);

            if($resultado == 1){
                return response()->json("ruta borrada");
            }else{
                return response()->json("No se ha podido borrar la ruta", 404);
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Elimina un ruta generica de la tabla rutas
     * TODO verificar si tiene registros de otras tablas vinculados, o si fallaría por integridad referencial
     */
    public function borrarRuta($idRuta)
    {
        try {
            
            $ruta = AdminRepository::obtenerRuta($idRuta);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($ruta)){

                $resultado = AdminRepository::borrarRuta($idRuta);

                if($resultado == 1){

                    return response()->json("ruta borrada");

                }else{

                    return response()->view('errors.404',['message'   => 'No se ha podido borrar la ruta'], 404);
                }
            }
            else{
                return response()->json("No existe ninguna ruta con el id: ".$idRuta, 404);
            }

        } 
        //Captura la excepcion lanzada al intentar borrar una ruta que tenga grupos_rutas_metodos asociados.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. La ruta que desea eliminar tiene grupos_rutas_metodos asociados.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /**
     * Lista todas las rutas
     */
    public function listarRutas()
    {
        try {
            return AdminRepository::listarRutas();
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    /**
     * Obtiene una ruta segun su ID.
     */
    public function obtenerRuta($idRuta)
    {
        try {

            $ruta = AdminRepository::obtenerRuta($idRuta);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($ruta)){
                return response()->json($ruta);
            }
            else{
                return response()->json("No existe ninguna ruta con el id: ".$idRuta, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Lista las rutas que tiene accesible el usuario
     */
    public function listarRutasUsuario(Request $request)
    {
        try {
            $usuario = $request->user();
            $idUsuario = $usuario->id;
            
            $rutasUsuario = AdminRepository::listarRutasUsuario($idUsuario);
            if(!empty($rutasUsuario)){
                return response()->json($rutasUsuario);
            }
            else{
                return response()->json("No existe ninguna ruta para el usuario ".$usuario->name, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     *************************** USUARIOS GRUPOS **************************
     */


    /*
     * Agrega un usuario a un grupo
     */    
    public function crearUsuarioGrupo(Request $request)
    {
        try {
            $datos = $request->all();

            if(AdminController::obtenerUsuarioId($request["idUsuario"]) != "[]"){

                if(!empty(AdminRepository::obtenerGrupo($request["idGrupo"]))){

                    Log::debug("Creando usuario-grupo: " . print_r($datos, true));
                    AdminRepository::crearUsuarioGrupo($datos);
                    return response()->json("Usuario agregado al Grupo con éxito");
                }
                else{
                    return response()->json("El Grupo no existe");
                }
            }
            else{
                return response()->json("El usuario no existe");
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Elimina un usuario de un grupo
     */  
    public function eliminarUsuarioGrupo(REQUEST $request)
    {
        try {
            $idUsuario = $request->id_usuario;
            $idGrupo = $request->id_grupo;

            Log::debug("Eliminando usuario-grupo: " . print_r($idUsuario, true));

            $resultado = AdminRepository::eliminarUsuarioGrupo($idUsuario, $idGrupo);

            if($resultado == 1){
                return response()->json("Usuario eliminado del Grupo con éxito");
            }
            else{
                return response()->json("El usuario no fue eliminado del Grupo", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Obtiene los registros de la tabla usuarios_grupos. Lista los usuarios y sus grupos.
     */
    public function obtenerUsuariosGrupos()
    {
        try {
            $resultado = AdminRepository::obtenerUsuariosGrupos();

            if(!empty($resultado)){
                return response()->json($resultado);
            }
            else{
                return response()->json("No existe ningun usuario y grupo", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Lista los usuarios de un grupo determinado.
     */  
    public function obtenerUsuariosDeGrupo($idGrupo)
    {
        try {
            //Primero verificamos que el grupo exista
            $grupo = AdminRepository::obtenerGrupo($idGrupo);
            //si $grupo no esta vacio, quiere decir que el grupo existe
            if(!empty($grupo)){
                //una vez validado que el grupo existe, obtenemos los usuarios del mismo.
                $resultado = AdminRepository::obtenerUsuariosDeGrupo($idGrupo);
                //validamos si el grupo posee usuarios asociados
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("El grupo no cuenta con ningun usuario asociado", 404);
                }
            }
            else{
                return response()->json("El grupo no existe", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Lista los grupos de un usuario determinado.
     */  
    public function obtenerGruposDeUsuario($idUsuario)
    {
        try {
            //Primero verificamos que el usuario exista
            $usuario = User::find($idUsuario);
            //si $usuario no esta vacio, quiere decir que el usuario existe
            if(!empty($usuario)){
                //una vez validado que el usuario existe, obtenemos los grupos del mismo.
                $resultado = AdminRepository::obtenerGruposDeUsuario($idUsuario);
                //validamos si el usuario posee grupos asociados
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("El usuario no cuenta con ningun grupo asociado", 404);
                }
            }
            else{
                return response()->json("El usuario no existe", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     *************************  GRUPOS RUTAS METODOS **************************
     */


    /*
     * Agrega una ruta a un grupo, en la tabla grupos_rutas_metodos.
     */ 
    public function crearGrupoRuta(Request $request)
    {
        try {
            $datos = $request->all();
            Log::debug("Creando grupo-ruta: " . print_r($datos, true));
            AdminRepository::crearGrupoRuta($datos);
            return response()->json("Ruta agregada al Grupo con éxito");
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Elimina una ruta de un grupo, en la tabla grupos_rutas_metodos.
     */ 
    public function eliminarGrupoRuta($idGrupoRutaMetodo)
    {
        try {
            Log::debug("Eliminando grupo-ruta: " . print_r($idGrupoRutaMetodo, true));
            $resultado = AdminRepository::eliminarGrupoRuta($idGrupoRutaMetodo);
            
            if($resultado == 1){

                return response()->json("Grupo-Ruta eliminado con éxito");
                
            }
            else{

                return response()->json("Grupo-Ruta no fue eliminado ", 404);
                
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Devuelve todas las rutas asociadas a los grupos creados.
     */ 
    public function obtenerGruposRutas()
    {
        try {

            $resultado = AdminRepository::obtenerGruposRutas();

            if(!empty($resultado)){
                return response()->json($resultado);

            }else{
                return response()->json("No existe ningun grupo-ruta", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Devuelve todas las rutas asociadas a un grupo especifico.
     */ 
    public function obtenerRutasDeGrupo($idGrupo)
    {
        try {

            //Primero verificamos que el grupo exista
            $grupo = AdminRepository::obtenerGrupo($idGrupo);
            //si $grupo no esta vacio, quiere decir que el grupo existe
            if(!empty($grupo)){
                //una vez validado que el grupo existe, obtenemos las rutas del mismo.
                $resultado = AdminRepository::obtenerRutasDeGrupo($idGrupo);
                //validamos si el grupo posee rutas asociadas
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("El grupo no cuenta con ninguna ruta asociada", 404);
                }
            }
            else{
                return response()->json("El grupo no existe", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Devuelve todos los grupos asociados a una ruta especifica.
     */ 
    public function obtenerGruposDeRuta($idRuta)
    {
        try {

            //Primero verificamos que la ruta exista
            $ruta = AdminRepository::obtenerRuta($idRuta);
            //si $ruta no esta vacio, quiere decir que la ruta existe
            if(!empty($ruta)){
                //una vez validado que la ruta existe, obtenemos los grupos de la misma.
                $resultado = AdminRepository::obtenerGruposDeRuta($idRuta);
                //validamos si la ruta posee grupos asociados
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("La ruta no cuenta con ningun grupo asociado", 404);
                }
            }
            else{
                return response()->json("La ruta no existe", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Valida que un password sea correcto.
     */ 
    public function validar_clave($clave,&$error_clave){
        if(strlen($clave) < 6){
           $error_clave = "La clave debe tener al menos 6 caracteres";
           return false;
        }
        if(strlen($clave) > 16){
           $error_clave = "La clave no puede tener más de 16 caracteres";
           return false;
        }
        if (!preg_match('`[a-z]`',$clave)){
           $error_clave = "La clave debe tener al menos una letra minuscula";
           return false;
        }
        if (!preg_match('`[A-Z]`',$clave)){
           $error_clave = "La clave debe tener al menos una letra mayuscula";
           return false;
        }
        if (!preg_match('`[0-9]`',$clave)){
           $error_clave = "La clave debe tener al menos un caracter numerico";
           return false;
        }
        $error_clave = "";
        return true;
     }
}
